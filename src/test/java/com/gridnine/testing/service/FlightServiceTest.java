package com.gridnine.testing.service;

import com.gridnine.testing.model.Flight;
import com.gridnine.testing.util.FlightBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FlightServiceTest {
    public static List<Flight> flightList = FlightBuilder.createFlights();
    public static FlightFilter flightFilter = new FlightService();
    LocalDateTime localDateTime = LocalDateTime.of(2014, 9, 19, 14, 5);

    @Test
    void getFlightList() {
        assertEquals(flightFilter.getFlightList(flightList, localDateTime).size(),
                5);
    }

    @Test
    void getBeforeFlightList() {
    }

    @Test
    void getTimeMoreTwoHoursExclude() {

    }
}