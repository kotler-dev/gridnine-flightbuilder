package com.gridnine;


import com.gridnine.testing.model.Flight;
import com.gridnine.testing.model.Segment;
import com.gridnine.testing.service.FlightFilter;
import com.gridnine.testing.service.FlightService;
import com.gridnine.testing.util.FlightBuilder;

import java.time.LocalDateTime;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Flight> flightList = FlightBuilder.createFlights();
        FlightFilter flightFilter = new FlightService();

        var filterFlightList = flightFilter.getFlightList(flightList, LocalDateTime.of(2014, 9, 19, 14, 5));
        filterFlightList.forEach(System.out::println);
    }
}
