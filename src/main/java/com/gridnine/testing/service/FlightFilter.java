package com.gridnine.testing.service;

import com.gridnine.testing.model.Flight;

import java.time.LocalDateTime;
import java.util.List;

public interface FlightFilter {
    List<Flight> getFlightList(List<Flight> flightList, LocalDateTime localDateTime);
    List<Flight> getBeforeFlightList(List<Flight> beforeFlightList);
    List<Flight> getTimeMoreTwoHoursExclude(List<Flight> flightList, int time);
}
