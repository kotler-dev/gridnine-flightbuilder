package com.gridnine.testing.service;

import com.gridnine.testing.model.Flight;
import com.gridnine.testing.model.Segment;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FlightService implements FlightFilter {
    @Override
    public List<Flight> getFlightList(List<Flight> flightList, LocalDateTime localDateTime) {
        List<Flight> result = new ArrayList<>(flightList);

        for (Flight flight : flightList) {
            for (Segment segment : flight.getSegments()) {
                if (segment.getDepartureDate().isBefore(localDateTime)) {
                    result.remove(flight);
                }
            }
        }
        return result;
    }

    @Override
    public List<Flight> getBeforeFlightList(List<Flight> dataFlightList) {
        List<Flight> resultBefore = new ArrayList<>(dataFlightList);

        for (Flight flight : dataFlightList) {
            for (Segment segment : flight.getSegments()) {
                if (segment.getArrivalDate().isBefore(segment.getDepartureDate())) {
                    resultBefore.remove(flight);
                }
            }
        }
        return resultBefore;
    }

    @Override
    public List<Flight> getTimeMoreTwoHoursExclude(List<Flight> flightList, int time) {
        List<Flight> resultBefore = new ArrayList<>(flightList);

        for (Flight flight : flightList) {
            List<Segment> segmentList = flight.getSegments();

            // Departure - вылет
            // Arrival - прилет
            int groundTime = 0;
            if (segmentList.size() > 1) {
                for (int i = 0; i < segmentList.size() - 1; i++) {
                    groundTime += Math.abs(segmentList.get(i + 1).getDepartureDate().getHour() - segmentList.get(i).getArrivalDate().getHour());
                    System.out.println("getDepartureDate -> " + segmentList.get(i + 1).getDepartureDate().getHour() + " getArrivalDate -> " + segmentList.get(i).getArrivalDate().getHour() + " groundTime -> " + groundTime);
                    if (groundTime > time) {
                        resultBefore.remove(flight);
                    }
                }
                System.out.println("asd");
            }
        }
        return resultBefore;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
